# RoomAdmin
Web interface for controlling and monitoring MQTT devices.

# Set up web server
```
sudo apt install apache2
cd /var/www/html
sudo rm -rf * .*
sudo git clone --recursive https://gitlab.com/NANDLAB/RoomAdmin.git .
```

## RoomAdmin Configuration
You can create a `config.js` file in the project directory. The default configuration can be found in the `main.js`. For example, to override the broker hostname configuration, write the following content in `config.js`:
```
var config = {
    broker: {
        host: mybroker
    }
}
```

# Set up MQTT Broker
The following commands will
- install the mosquitto broker
- configure him to open a TCP socket on port 1883 and a websocket on port 9001
```
sudo apt install mosquitto
sudo sh -c "printf 'listener 1883\nlistener 9001\nprotocol websockets\nallow_anonymous true\n' > /etc/mosquitto/conf.d/sockets.conf"
sudo systemctl restart mosquitto
```

# Test MQTT Communication
The `mosquitto-clients` package contains command line tools for publishing and subscribing to mqtt topics. You can install it with `sudo apt install mosquitto-clients`.

## Examples
- Subscribe: `mosquitto_sub -t control/switches`
- Publish: `mosquitto_pub -t control/signal -m RESET`
